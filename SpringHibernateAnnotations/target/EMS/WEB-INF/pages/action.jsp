<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Action on Employee Management System</title>
</head>
<body>
	<center>

		<div style="color: teal; font-size: 30px">Select Action on Employee Management System</div>
		<div style="margin-left: 5px">
			<form action="menu" method="post" >
			<br/>select your action:
        	<br/><input type="radio" name=myaction value="1"/>Search Employee
        	<br/><input type="radio" name=myaction value="2"/>Add    Employee
        	<br/><input type="radio" name=myaction value="3"/>Edit   Employee
        	<br/><input type="radio" name=myaction value="4"/>Delete Employee
        	
        	<br/><input type="submit" value="Action">
        	</form>
		</div>	

	</center>
</body>
</html>
