<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Employee Details</title>
</head>
<body>
	<center>

		<div style="color: teal; font-size: 30px">Employee Search Details</div>

		<c:if test="${!empty employeeList}">
			<table border="1" bgcolor="black" width="600px">
				<tr
					style="background-color: teal; color: white; text-align: center;"
					height="40px">
					<td>EmpId</td>
					<td>Name</td>
					<td>Title</td>
					<td>Location</td>
					<td>Email</td>
					<td>Phone</td>
				</tr>
				<c:forEach items="${employeeList}" var="user">
					<tr
						style="background-color: white; color: black; text-align: center;"
						height="30px">
						<td><c:out value="${user.id}" />
						</td>
						<td><c:out value="${user.name}" />
						</td>
						<td><c:out value="${user.title}" />
						</td>
						<td><c:out value="${user.location}" />
						</td>
						<td><c:out value="${user.email}" />
						</td>
						<td><c:out value="${user.phone}" />
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>


		<a href="action">Go Back to Menu</a>
	</center>
</body>
</html>
