<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>EditDelete On Employee Details</title>
</head>
<body>
	<center>

		<div style="color: teal; font-size: 30px">Action on ${msg}</div>



		<form:form id="searchForm" modelAttribute="employee" method="post" action="edit">
			<table width="400px" height="150px">
				<tr>
					<td><form:label path="id">Enter employee Id</form:label></td>
					<td><form:input path="id" value="0"/></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Submit" />
					</td>
				</tr>
			</table>
		</form:form>


		<a href="action">Go Back to Menu</a>
	</center>
</body>
</html>
