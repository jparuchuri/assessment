<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Being Java Guys | Edit User Details</title>
</head>
<body>
	<center>

		<div style="color: teal; font-size: 30px">
			Edit Employee Details</div>



		<form:form id="registerForm" modelAttribute="employee" method="post"
			action="update">
			<table width="400px" height="150px">
				<tr>
					<td><form:label path="id">EmpId</form:label></td>
					<td><form:input path="id" value="${employeeObject.id}" readonly="true"/></td>
				</tr>
				<tr>
					<td><form:label path="name">Name</form:label></td>
					<td><form:input path="name" value="${employeeObject.name}" /></td>
				</tr>
				<tr>
					<td><form:label path="title">Title</form:label></td>
					<td><form:input path="title" value="${employeeObject.title}"/></td>
				</tr>
				<tr>
					<td><form:label path="location">Title</form:label></td>
					<td><form:input path="location" value="${employeeObject.location}"/></td>
				</tr>
				<tr>
					<td><form:label path="email">Email</form:label></td>
					<td><form:input path="email" value="${employeeObject.email}"/></td>
				</tr>
				<tr>
					<td><form:label path="phone">Phone</form:label></td>
					<td><form:input path="phone" value="${employeeObject.phone}"/></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Update" />
					</td>
				</tr>
			</table>
		</form:form>

<a href="action">Go Back to Menu</a>
		
	</center>
</body>
</html>
