package com.ems.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ems.dao.EMSDao;
import com.ems.domain.Employee;

public class EMSServiceImpl implements EMSService {
	
	@Autowired
	EMSDao emsDao;

	@Override
	public int insertRow(Employee employee) {
		return emsDao.insertRow(employee);
	}

	@Override
	public List<Employee> getList() {
		return emsDao.getList();
	}

	@Override
	public Employee getRowById(int id) {
		return emsDao.getRowById(id);
	}

	@Override
	public int updateRow(Employee employee) {
		return emsDao.updateRow(employee);
	}

	@Override
	public int deleteRow(int id) {
		return emsDao.deleteRow(id);
	}

	@Override
	public List<Employee> searchOnOptions(Employee employee) {
		// TODO Auto-generated method stub
		return emsDao.getByOptions(employee);
	}

}
