package com.ems.services;

import java.util.List;

import com.ems.domain.Employee;

public interface EMSService {
	public int insertRow(Employee employee);

	public List<Employee> getList();

	public Employee getRowById(int id);

	public int updateRow(Employee employee);
	
	public List<Employee> searchOnOptions(Employee employee);

	public int deleteRow(int id);

}
