package com.ems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ems.domain.Employee;
import com.ems.services.EMSService;

@Controller
public class EMSController {

	@Autowired
	EMSService emsService;

	@RequestMapping("home")
	public String getHome() {
		return "home";
	}

	@RequestMapping("login")
	public ModelAndView getLoginForm(
			@RequestParam(required = false) String authfailed, String logout) {
		String message = "";
		if (authfailed != null) {
			message = "Invalid username of password, try again !";
		} else if (logout != null) {
			message = "Logged Out successfully, login again to continue !";
		}
		return new ModelAndView("login", "message", message);
	}

	@RequestMapping("profile")
	public String geProfilePage() {
		return "profile";
	}

	@RequestMapping("form")
	public ModelAndView getForm(@ModelAttribute Employee employee) {
		return new ModelAndView("form");
	}

	@RequestMapping("menu")
	public ModelAndView menu(@RequestParam int myaction,
			@ModelAttribute Employee employee) {
		if (myaction == 1) {
			return new ModelAndView("searchOptions");
		} else if (myaction == 2) {
			return new ModelAndView("redirect:form");
		} else if (myaction == 3) {
			return new ModelAndView("editDelete", "msg", "Edit");
		} else if (myaction == 4) {
			return new ModelAndView("deleteId");
		} else {
			return new ModelAndView("action");
		}
	}

	@RequestMapping("action")
	public ModelAndView action() {
		return new ModelAndView("action");
	}

	@RequestMapping("register")
	public ModelAndView registerUser(@ModelAttribute Employee employee) {
		int ret = emsService.insertRow(employee);
		if (ret >= 0) {
			return new ModelAndView("successMsg", "msg", " Successfully Added");
		} else {
			return new ModelAndView("successMsg", "msg", " Couldnot be Added");
		}
	}

	@RequestMapping("list")
	public ModelAndView getList() {
		List employeeList = emsService.getList();
		return new ModelAndView("list", "employeeList", employeeList);
	}

	@RequestMapping("delete")
	public ModelAndView deleteUser(@ModelAttribute Employee employee) {
		int ret = emsService.deleteRow(employee.getId());
		if (ret >= 0) {
			return new ModelAndView("successMsg", "msg",
					" Successfully Deleted");
		} else {
			return new ModelAndView("successMsg", "msg", " Couldnot be Deleted");
		}
	}

	@RequestMapping("edit")
	public ModelAndView editUser(@ModelAttribute Employee employee) {
		Employee employeeObject = emsService.getRowById(employee.getId());
		if (employeeObject != null) {
			return new ModelAndView("edit", "employeeObject", employeeObject);
		} else {
			return new ModelAndView("successMsg", "msg", "Couldnot be Updated");
		}
	}

	@RequestMapping("update")
	public ModelAndView updateUser(@ModelAttribute Employee employee) {
		int ret = emsService.updateRow(employee);
		if (ret >= 0) {
			return new ModelAndView("successMsg", "msg", "Successfully Updated");
		} else {
			return new ModelAndView("successMsg", "msg", "Couldnot be Updated");
		}
	}

	@RequestMapping("searchOptionsService")
	public ModelAndView searchOptions(@ModelAttribute Employee employee) {
		List<Employee> employeeList = emsService.searchOnOptions(employee);
		employeeList.add(employee);
		return new ModelAndView("list", "employeeList", employeeList);
	}

}
