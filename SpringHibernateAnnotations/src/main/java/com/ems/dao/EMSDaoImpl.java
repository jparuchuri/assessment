package com.ems.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.ems.domain.Employee;

public class EMSDaoImpl implements EMSDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	@Transactional
	public int insertRow(Employee employee) {
		try{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(employee);
		tx.commit();
		Serializable id = session.getIdentifier(employee);
		session.close();
		return (Integer) id;
		}catch(Exception e){
			return -1;
		}
	}

	@Override
	public List<Employee> getList() {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Employee> employeeList = session.createQuery("from Employee")
				.list();
		session.close();
		return employeeList;
	}

	@Override
	public Employee getRowById(int id) {
		try{
		Session session = sessionFactory.openSession();
		Employee employee = (Employee) session.load(Employee.class, id);
		if(employee!=null && employee.getId()>0){
		return employee;
		}else{
			return null;
		}
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public int updateRow(Employee employee) {
		try{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(employee);
		tx.commit();
		Serializable id = session.getIdentifier(employee);
		session.close();
		return (Integer) id;
		}catch(Exception e){
			return -1;
		}
	}

	@Override
	public int deleteRow(int id) {
		try{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Employee employee = (Employee) session.load(Employee.class, id);
		session.delete(employee);
		tx.commit();
		Serializable ids = session.getIdentifier(employee);
		session.close();
		return (Integer) ids;}
		catch(Exception e){
			return -1;
		}
	}

	@Override
	public List<Employee> getByOptions(Employee employee) {
		Session session = sessionFactory.openSession();
		String query = "select * from Employee e where";
		int temp =0;
				
				
				if(employee.getName()!=null && !employee.getName().trim().equals("")){
					query+=" e.name like '%"+employee.getName().trim()+"%'";
						query += " and";
					temp++;
				} if(employee.getTitle()!=null && !employee.getTitle().trim().equals("")){
					query+=" e.title like '%"+employee.getTitle().trim()+"%'";
						query += " and";
					temp++;
				}if(employee.getLocation()!=null && !employee.getLocation().trim().equals("")){
					query+=" e.location like '%"+employee.getLocation().trim()+"%'";
					query += " and";
				temp++;
				} if(employee.getEmail()!=null && !employee.getEmail().trim().equals("")){
					query+=" e.email like '%"+employee.getEmail().trim()+"%'";
						query += " and";
					temp++;
				} if(employee.getPhone()!=null && !employee.getPhone().trim().equals("")){
					query+=" e.phone like '%"+employee.getPhone().trim()+"%'";
						query += " and";
					temp++;
				}
				query = query.substring(0, query.length()-4);
		if(temp==0){
			return new ArrayList<Employee>();
		}else{
			Query querystm = session.createSQLQuery(query).addEntity(Employee.class);
			List<Employee> result = querystm.list();
			return result;
		}
			
		
		
	}

}
