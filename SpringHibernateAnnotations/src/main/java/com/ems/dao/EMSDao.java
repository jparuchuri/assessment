package com.ems.dao;

import java.util.List;

import com.ems.domain.Employee;

public interface EMSDao {
	public int insertRow(Employee employee);

	public List<Employee> getList();

	public Employee getRowById(int id);

	public int updateRow(Employee employee);
	
	public List<Employee> getByOptions(Employee employee);

	public int deleteRow(int id);

}
