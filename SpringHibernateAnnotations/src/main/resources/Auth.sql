CREATE TABLE IF NOT EXISTS `users` (  
  `username` varchar(45) NOT NULL UNIQUE,
  `password` varchar(450) NOT NULL,  
  `enabled` tinyint(4) NOT NULL DEFAULT '1', 
  `name` varchar(45) DEFAULT NULL,  
  `title` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,  
  `email` varchar(45) DEFAULT NULL,  
  `phone` varchar(45) DEFAULT NULL,   
  PRIMARY KEY (`username`)  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;  
  

CREATE TABLE IF NOT EXISTS `user_roles` (  
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,  
  `username` varchar(45) NOT NULL,  
  `ROLE` varchar(45) NOT NULL,  
  PRIMARY KEY (`user_role_id`),  
  UNIQUE KEY `uni_username_role` (`ROLE`,`username`),  
  KEY `fk_username_idx` (`username`),  
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`)  
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;  


CREATE DATABASE IF NOT EXISTS `springhibernate_db` /*!40100 DEFAULT CHARACTER SET latin1 */;  
USE `springhibernate_db`; 

INSERT INTO users(username,password,enabled, name, title, location, email, phone)
VALUES ('jack','123456', TRUE, 'Jack', 'HR', 'Aus','jack@gmail.com', '123456789');
INSERT INTO users(username,password,enabled, name, title, location, email, phone)
VALUES ('alex','123456', TRUE, 'Alex','Developer','Dall', 'alex@gmail.com','12345');
 
INSERT INTO user_roles (username, ROLE)
VALUES ('jack', 'ROLE_USER');
INSERT INTO user_roles (username, ROLE)
VALUES ('jack', 'ROLE_ADMIN');
INSERT INTO user_roles (username, ROLE)
VALUES ('alex', 'ROLE_USER');