CREATE DATABASE IF NOT EXISTS `springhibernate_db` /*!40100 DEFAULT CHARACTER SET latin1 */;  
USE `springhibernate_db`;  




CREATE TABLE IF NOT EXISTS `employee` (  
  `id` int(11) NOT NULL AUTO_INCREMENT,  
  `pwd` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,  
  `title` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,  
  `email` varchar(45) DEFAULT NULL,  
  `phone` varchar(45) DEFAULT NULL,  
  PRIMARY KEY (`id`)  
) ;

INSERT INTO `employee` (`id`, `pwd`, `name`, `title`,`location`, `email`, `phone`) VALUES  
 (10, 'AA','Virat', 'HR','Austin,TX', 'virat@beingjavaguys.com', '89876787890'),  
 (11, 'AA','Sachin', 'Engineer','Dallas, TX', 'sachin@india.com', '89898989898'),  
 (12, 'AA','Virendra', 'CEO', 'Houston,TX','viru@delhi.com', '8976778789');  