package ems;

import java.util.List;
import java.util.Scanner;


public class EMSAction {
	
	/**
	 * Show the transaction history for an account.
	 * 
	 * @param theUser
	 *            the logged-in User object
	 * @param sc
	 *            the Scanner object used for user input
	 */
	public void searchEmployee(List<Integer> choice, List<String> searchKeyword,
			EMSConnection connection, Scanner sc) {
		try {
		
			List<Employee> employeeList = connection.search(choice, searchKeyword);
			System.out.println("----------------------------------------------------------");
			System.out.println("EmpId\t Name\t Title\t Location\t Email\t PhoneNumber");
			for(Employee emp : employeeList){
				System.out.println(emp.getEmpId()+"\t"+emp.getName()+"\t"+emp.getTitle()+"\t"+emp.getLocation()+"\t"+emp.getEmail()+"\t"+emp.getPhoneNumber());
			}
		} catch (Exception e) {
			System.out.println("Please check the Employee Details.");
		}

	}	
	
	public void deleteEmp(EMSConnection connection, int empId){
		try {
			
			boolean result = connection.deleteEmp(empId);
			if(result){
				System.out.println("Successfully removed employee with Id :"+empId+" from the records.");
			}else{
				System.out.println("Cannot remove employee with Id :"+empId+" from records. Please contact admin.");
			}
		} catch (Exception e) {
			System.out.println("Please check the Employee Details.");
		}
	}
	
	public void updateEmp(EMSConnection connection, int empId, List<Integer> choice, List<String> updateList){
		try {
			
			boolean result = connection.updateEmp(empId, choice, updateList);
			if(result){
				System.out.println("Successfully Update details of employee with Id :"+empId+" in the records.");
			}else{
				System.out.println("Cannot update details of  employee with Id :"+empId+" in records. Please contact admin.");
			}
		} catch (Exception e) {
			System.out.println("Please check the Employee Details.");
		}
	}
	
	public void addEmp(EMSConnection connection, List<String> updateList){
		try {
			
			int empId = connection.addEmp(updateList);
			if(empId!=0){
				System.out.println("Successfully Inserted details of employee with Id :"+empId+" in the records.");
			}else{
				System.out.println("Cannot create employee record at this time. Please contact admin.");
			}
		} catch (Exception e) {
			System.out.println("Please check the Employee Details.");
		}
	}
}
