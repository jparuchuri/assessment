package ems;

public class Employee {
	
	int empId;
	
	String name;
	
	String title;
	
	String location;
	
	String email;
	
	String phoneNumber;
	
	Employee(int empId, String name, String title, String locaton, String email, String phoneNumber){
		this.empId = empId;
		this.name = name;
		this.title = title;
		this.location = locaton;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	

}
