package ems;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class EMS {
	public static void main(String[] args) {
		// Scanner
		Scanner sc = new Scanner(System.in);

		Employee authorizedUser = null;

		EMS ems = new EMS();

		// Show message till user chooses to Quit.
		while (true) {

			// Welcome messgae and Authorization
			authorizedUser = ems.welcomeAndAuth(sc);

			// Prompt menu till user chooses to Quit
			ems.showMenu(authorizedUser, sc);

		}
	}

	/**
	 * Welcome message and Authorization.
	 * 
	 * @param sc
	 *            Object to get the input
	 */
	public synchronized Employee welcomeAndAuth(Scanner sc) {
		int pwd = 0;
		int empId = 0;
		Employee userAuth = null;
		EMSConnection connection = new EMSConnection();

		System.out.printf("\nWelcome to Employee Management System.\n\n");

		do {
			System.out.println("-----------------------");
			System.out.print("Please Enter EmployeeId: ");
			try {
				empId = sc.nextInt();
				System.out.print("Please Enter 4 digit Password: ");
				pwd = sc.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Please Enter Number Only");
				sc.next();
				continue;
			} catch (Exception ex) {
				System.out
						.println("System is temporarily facing issue. Please try again later.");
			}
			userAuth = connection.authenticate(empId, pwd);

			if (userAuth != null) {
				System.out.println("You have successfully logged in "
						+ userAuth.getName());
			} else {
				System.out
						.println("Incorrect EmployeeId and Password combination. Please try again.");
			}

		} while (userAuth == null);

		return userAuth;

	}

	/**
	 * Show Menu for user activity.
	 * 
	 * @param theUser
	 *            object for logged-in user.
	 * @param sc
	 *            object to get user input
	 */
	public void showMenu(Employee customer, Scanner sc) {
		EMSConnection connection = new EMSConnection();
		EMSAction action = new EMSAction();
		int choice = 0;

		// main menu
		do {
			System.out.println("---------------------");
			System.out.println("Main Menu");
			System.out.println("  1) Search Employee");
			if (customer.getTitle().equals("HR")) {
				System.out.println("  2) Add New Employee");
				System.out.println("  3) Edit Employee");
				System.out.println("  4) Delete Employee");
				System.out.println("  5) Quit");
			} else {
				System.out.println("  2) Quit");
			}
			System.out.println();
			System.out.print("Enter your choice: ");
			try {
				choice = sc.nextInt();
			} catch (InputMismatchException e) {
				sc.nextLine();
			}

			if ((choice < 1 || choice > 5) && customer.getTitle().equals("HR")) {
				System.out.println("Please choose a valid choice between 1-5.");
			} else if ((choice < 1 || choice > 2)
					&& !customer.getTitle().equals("HR")) {
				System.out.println("Please choose a valid choice between 1-2.");

			}

		} while (((choice < 1 || choice > 5) && customer.getTitle()
				.equals("HR"))
				|| ((choice < 1 || choice > 2) && !customer.getTitle().equals(
						"HR")));

		if (!customer.getTitle().equals("HR") && choice == 2) {
			choice = 5;
		}
		String moreSearch = "N";
		String searchKeyword;
		List<String> listKeyword = new ArrayList<String>();
		List<Integer> listChoice = new ArrayList<Integer>();
		int empId = 0;
		// process the choice
		switch (choice) {

		case 1:
			// show more info to search on keyword.
			listChoice = new ArrayList<Integer>();
			listKeyword = new ArrayList<String>();
			do {
				System.out.println("---------------------");
				System.out.println("Search Menu");
				System.out.println("  1) Search on EmpId");
				System.out.println("  2) Search on Name");
				System.out.println("  3) Search on Title");
				System.out.println("  4) Search on Location");
				System.out.println("  5) Search on Email");
				System.out.println("  6) Search on PhoneNumber");
				System.out.println();
				System.out.print("Enter your choice: ");
				try {
					choice = sc.nextInt();
					if (choice < 1 || choice > 6) {
						System.out
								.println("Please choose a valid choice between 1-6.");
						sc.nextLine();
					} else {
						listChoice.add(choice);
						System.out.println();
						System.out.print("Enter your search keyword: ");
						searchKeyword = sc.next();
						listKeyword.add(searchKeyword);
						System.out
								.println("Do you want to search on another keyword:(Press Y for Yes, Anyother key for NO.)");
						moreSearch = sc.next();
					}
				} catch (InputMismatchException e) {
					System.out
					.println("Please choose a valid choice between 1-6.");
					sc.nextLine();
				}

			} while (choice < 1 || choice > 6
					|| moreSearch.equalsIgnoreCase("Y"));
			action.searchEmployee(listChoice, listKeyword, connection, sc);
			break;
		case 2:
			// Enter Employee record to DB.
			listKeyword = new ArrayList<String>();
			System.out.println("---------------------");
			System.out.println("Enter Employee Details to be added:");

			System.out.println(" Enter Name :");
			listKeyword.add(sc.next());
			System.out.println(" Enter Title");
			listKeyword.add(sc.next());
			System.out.println(" Enter Location");
			listKeyword.add(sc.next());
			System.out.println(" Enter Email");
			listKeyword.add(sc.next());
			System.out.println(" Enter PhoneNumber");
			listKeyword.add(sc.next());

			action.addEmp(connection, listKeyword);
			break;
		case 3:
			// show more info to search on keyword.
			listChoice = new ArrayList<Integer>();
			listKeyword = new ArrayList<String>();
			System.out.println("---------------------");
			System.out.println("Enter EmployeeId to be updated:");
			try {
				empId = sc.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Wrong Input. Please try again.");
				sc.nextLine();
				break;
			}
			do {
				try {
					System.out.println("Update Menu");
					System.out.println("  1) Update on Name");
					System.out.println("  2) Update on Title");
					System.out.println("  3) Update on Location");
					System.out.println("  4) Update on Email");
					System.out.println("  5) Update on PhoneNumber");
					System.out.println();
					System.out.print("Enter your choice: ");
					choice = sc.nextInt();
					if (choice < 1 || choice > 5) {
						System.out
								.println("Please choose a valid choice between 1-5.");
						sc.next();
					} else {
						listChoice.add(choice);
						System.out.println();
						System.out.print("Enter your value to be updated: ");
						try {
							searchKeyword = sc.next();
							listKeyword.add(searchKeyword);
						} catch (InputMismatchException e) {
							sc.nextLine();
						}

						System.out
								.println("Do you want to update another field:(Y/N)");
						moreSearch = sc.next();
					}

				} catch (InputMismatchException e) {
					System.out.println("Please enter a valid Number");
					choice = 0;
					sc.nextLine();
				}

			} while (choice < 1 || choice > 5
					|| moreSearch.equalsIgnoreCase("Y"));
			action.updateEmp(connection, empId, listChoice, listKeyword);
			break;
		case 4:
			System.out.println("Enter EmployeeId to be deleted:");
			empId = sc.nextInt();
			action.deleteEmp(connection, empId);
			break;
		case 5:
			sc.nextLine();
			System.out
					.println("You have successfully logged out. Thank you for using Employee Management System.");
			break;
		}

		// repeat the main menu until user chooses to quit.
		if (choice != 5) {
			showMenu(customer, sc);
		}

	}

}
