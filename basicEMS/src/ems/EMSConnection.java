package ems;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.Statement;

public class EMSConnection {
	private Connection connect = null;
	private PreparedStatement preparedStatement = null;
	private Statement statement = null;
	private ResultSet resultSet = null;

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/mysql";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "";

	/**
	 * Get the connection for MYSQL.
	 */
	public Connection getConnection() {
		Connection connector = null;
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connector = DriverManager.getConnection("jdbc:mysql://localhost/mysql?user=root&password=");

		} catch (Exception e) {
			System.out.println("Server is down. Please contact admin.");
		}
		return connector;
	}

	/**
	 * Authenticate the CustomerId and Pin.
	 * 
	 * @param customerId
	 *            unique Id for the customer.
	 * @param pin
	 *            pin to authenticate user
	 * 
	 */
	public Employee authenticate(int employeeId, int password) {
		Employee employee = null;

		try {

			connect = getConnection();
			// Statements allow to issue SQL queries to the database
			preparedStatement = connect
					.prepareStatement("select * from EMS where EmpId=? and Password=?;");
			preparedStatement.setInt(1, employeeId);
			preparedStatement.setInt(2, password);
			// Result set get the result of the SQL query
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null && resultSet.next()) {
				employee = new Employee(employeeId,
						resultSet.getString("Name"),
						resultSet.getString("Title"),
						resultSet.getString("Location"),
						resultSet.getString("Email"),
						resultSet.getString("PhoneNumber"));
			}
			return employee;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Server is down. Please contact admin.");
			return null;
		} finally {
			close(resultSet, preparedStatement, connect);
		}

	}
	
	/**
	 * Search Employee record in the database
	 * 
	 * @param choice
	 *            List of choices to be searched upon
	 * @param searchKeyword
	 *            List of keywords to be searched upon
	 * 
	 */
	public List<Employee> search(List<Integer> choice, List<String> searchKeyword) {
		List<Employee> employeeList = new ArrayList<Employee>();

		try {

			connect = getConnection();
			String query = "select * from EMS where";
			int temp =0;
			for(int id=0;id<choice.size();id++){
				if(!searchKeyword.get(id).trim().equals("")){
					
					if(temp>0){
						query += " and";
					}
					if(choice.get(id)==1){
						query+=" EmpId ="+searchKeyword.get(id).trim();
						temp++;
					}else if(choice.get(id)==2){
						query+=" Name like '%"+searchKeyword.get(id).trim()+"%'";
						temp++;
					}else if(choice.get(id)==3){
						query+=" Title like '%"+searchKeyword.get(id).trim()+"%'";
						temp++;
					}else if(choice.get(id)==4){
						query+=" Location like '%"+searchKeyword.get(id).trim()+"%'";
						temp++;
					}else if(choice.get(id)==5){
						query+=" Email like '%"+searchKeyword.get(id).trim()+"%'";
						temp++;
					}else if(choice.get(id)==6){
						query+=" PhoneNumber like '%"+searchKeyword.get(id).trim()+"%'";
						temp++;
					}
					
				
				}
			}
			// Statements allow to issue SQL queries to the database
			statement = (Statement) connect.createStatement();
			
			// Result set get the result of the SQL query
			resultSet = statement.executeQuery(query);

			while (resultSet != null && resultSet.next()) {
				Employee employee = new Employee(resultSet.getInt("EmpId"),
						resultSet.getString("Name"),
						resultSet.getString("Title"),
						resultSet.getString("Location"),
						resultSet.getString("Email"),
						resultSet.getString("PhoneNumber"));
				employeeList.add(employee);
			}
			return employeeList;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Server is down. Please contact admin.");
			return null;
		} finally {
			close(resultSet, preparedStatement, connect);
		}

	}

	/**
	 * Remove the employee record from the Database
	 * 
	 * @param empId
	 *            unique Id for the employee.
	 * 
	 */
	public boolean deleteEmp(int empId) {

		try {

			connect = getConnection();
			preparedStatement = connect.prepareStatement("delete from EMS where EmpId=?");
			preparedStatement.setInt(1, empId);
			// Result set get the result of the SQL query
			preparedStatement.executeUpdate();

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Server is down. Please contact admin.");
			return false;
		} finally {
			close(resultSet, preparedStatement, connect);
		}

	}
	
	/**
	 * Update the employee record in the Database
	 * 
	 * @param empId
	 *            unique Id for the employee.
	 * 
	 */
	public int addEmp(List<String> updateList) {

		int empId = 0;
		try {
			
			connect = getConnection();
			PreparedStatement prepSt = connect.prepareStatement("select EmpId from EMS order by EmpId desc limit 1");
			ResultSet rs = prepSt.executeQuery();
			if(rs!=null && rs.next()){
				empId = rs.getInt("EmpId")+1;
			}
			if(updateList!=null && updateList.size()==5){
			preparedStatement = connect.prepareStatement("insert into EMS values(?,?,?,?,?,?,?)");
			preparedStatement.setInt(1, empId);
			preparedStatement.setInt(2, empId);
			preparedStatement.setString(3, updateList.get(0));
			preparedStatement.setString(4, updateList.get(1));
			preparedStatement.setString(5, updateList.get(2));
			preparedStatement.setString(6, updateList.get(3));
			preparedStatement.setString(7, updateList.get(4));
			preparedStatement.executeUpdate();
			// Result set get the result of the SQL query
			}
			return empId;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Server is down. Please contact admin.");
			return empId;
		} finally {
			close(resultSet, preparedStatement, connect);
			
		}

	}
	
	/**
	 * Update the employee record in the Database
	 * 
	 * @param empId
	 *            unique Id for the employee.
	 * 
	 */
	public boolean updateEmp(int empId, List<Integer> choice, List<String> updateList) {

		try {

			connect = getConnection();
			String query = "update EMS set ";
			int temp =0;
			for(int id=0;id<choice.size();id++){
				
					if(temp>0){
						query += " ,";
					}
					if(choice.get(id)==1){
						query+=" Name ='"+updateList.get(id).trim()+"'";
						temp++;
					}else if(choice.get(id)==2){
						query+=" Title ='"+updateList.get(id).trim()+"'";
						temp++;
					}else if(choice.get(id)==3){
						query+=" Location ='"+updateList.get(id).trim()+"'";
						temp++;
					}else if(choice.get(id)==4){
						query+=" Email ='"+updateList.get(id).trim()+"'";
						temp++;
					}else if(choice.get(id)==5){
						query+=" PhoneNumber ='"+updateList.get(id).trim()+"'";
						temp++;
					}
			}
			query+=" where EmpId='"+empId+"'";
			statement = (Statement) connect.createStatement();
			statement.executeUpdate(query);
			// Result set get the result of the SQL query

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Server is down. Please contact admin.");
			return false;
		} finally {
			close(resultSet, preparedStatement, connect);
		}

	}
	
	/**
	 * Close the resultset.
	 */
	private void close(ResultSet resultSet,
			PreparedStatement preparedStatement, Connection connect) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (SQLException e) {

		}
	}

}